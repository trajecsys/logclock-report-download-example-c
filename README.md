# README #

## About this API ##

This is a bare-minimum C# example of exporting LogClock Reports.

### Summary ###

If you don't know what this is for, it's probably not for you.  This is an example of using C#'s WebClient to login to the Trajecsys system and download the LogClock csv/json report.  There are no credentials or accounts in this file, and creating a coordinator/admin account is not possible for the public.  That makes this app useless to anyone but those it has been provided for.

### How do I get set up? ###

You should be able to just import this repository into a newer Visual Studio with the Team Explorer.  If not, check it out and open the LogClockReportExample.sln in Visual Studio.

There should be NO additional setup steps.  I intentionally avoided the need for any third-party libraries from NuGet.

### How do I use it? ###

You really don't want to use this directly, except to understand the code.  If you fill out all fields with a valid coordinator/admin login in the Username/Password field, it will download a .csv or json report of all clock-in/out entries.

As a developer/IT person, the important class to be aware of is ReportHelper.cs.  Along with WebClientEx.cs (a subclass to preserve session in WebClient), those files provide the core functionality you will need to implement any automation on the download.

### How do I NOT use it? ###

We recommend you do not use this tool as a final product.  There is no error handling, and the manual data entry from the GUI leaves a lot of opportunity for error.  You should instead create a scheduled task that provides sufficient credentials and runs the export automatically.  Feel free to change the "Download" functionality to load the data in your own app and process it manually.

### Data ###

Be aware, this section may be out of date at any time.  It's here to help you, but not an authority on the final data format of the report.

At this time, the report includes the following fields, in order (with a header row describing the fields):

* student_id
* last_name
* first_name
* date
* time_in
* time_out
* time_total

## API Specification ##

### Login ###
This route will log you into a user using a session cookie.  

Please note, you should ignore the response body of this route if it returns a non-error code.  It is the same route used
internally to login to the system.

**URL** : `https://www.trajecsys.com/programs/api/users/Login.ashx`
**Method** : `POST`
**Auth Required** : NO
**Permissions Required** : Coordinator or Higher

**POST Parameters**
* **username** - *string* - The username of a program-privileged user.
* **password** - *string* - The matching password

You need to maintain or forward the request session cookie to calls to LogClock below.

### LogClock ###

**URL** : `https://www.trajecsys.com/programs/api/logclocks/Export.ashx`
**Method** : `GET`
**Auth Required** : YES
**Permissions Required** : Coordinator or Higher

**Query Parameters**
* **date_from** - *MM/dd/yyyy* - date range filter
* **date_to** - *MM/dd/yyyy* - date range filter
* **area_id** - *int* - area filter
* **format** - *string* - Supports `csv` (default) or `json`.  If invalid, we use `csv`

**Response Fields**
These fields are the same in JSON or CSV

* student_id - Your program's identifier for the student
* last_name - Student Last Name
* first_name - Student First Name
* date - Date of record
* time_in - Clock-in time
* time_out - Clock-out time
* time_total - Total time in minutes for this clock record

## Who do I talk to? ##

You can reach me with any questions at [it@trajecsys.com](mailto:it@trajecsys.com)