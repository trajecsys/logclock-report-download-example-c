﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LogClockReportExample
{
    public class ReportHelper
    {
        WebClientEx client;

        public ReportHelper(string username, string password)
        {             
            client = new WebClientEx();
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            var reqparm = new System.Collections.Specialized.NameValueCollection();
            reqparm.Add("username", username);
            reqparm.Add("password", password);

            byte[] responsebytes = client.UploadValues("https://www.trajecsys.com/programs/api/users/Login.ashx", "POST", reqparm);
            string responsebody = Encoding.UTF8.GetString(responsebytes);
            //client.Login("https://www.trajecsys.com/programs/api/users/Login.ashx", reqparm);

        }

        public string GetReport(string filePath, DateTime? startDate = null, DateTime? endDate = null, string format = "csv")
        {
            var start = DateTime.Today.Subtract(TimeSpan.FromDays(30));
            var end = DateTime.Today;
            if (startDate.HasValue) start = startDate.Value;
            if (endDate.HasValue) end = endDate.Value;

            var exportURL = "https://www.trajecsys.com/programs/api/logclocks/Export.ashx";

            var exportURI = new UriBuilder(exportURL);

            var formatter = "";
            if (format == "json")
            {
                formatter = "&format=json";
            }

            exportURI.Query =
                "date_from=" + start.ToString("MM/dd/yyyy") +
                "&date_to=" + end.ToString("MM/dd/yyyy") +
                formatter;            

            client.DownloadFile(exportURI.ToString(), filePath);

            return filePath;
        }
    }
}
