﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogClockReportExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dtStart.Value = DateTime.Today.Subtract(TimeSpan.FromDays(30));
            dtEnd.Value = DateTime.Today;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            var btnText = btnRun.Text;
            btnRun.Text = "Please Wait... ";
            btnRun.Enabled = false;

            var userName = txtUserName.Text;
            var password = txtPassword.Text;

            var startDate = dtStart.Value;
            var endDate = dtEnd.Value;

            var saveFile = txtSaveFile.Text;

            var reportHelper = new ReportHelper(userName, password);

            if(cbJSON.Checked)
            {
                reportHelper.GetReport(saveFile, startDate, endDate, "json");
            }
            else {
                reportHelper.GetReport(saveFile, startDate, endDate);
            }
            


            btnRun.Text = btnText;
            btnRun.Enabled = true;

            MessageBox.Show("Download Complete");



        }

        private void btnFilePick_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "CSV File|*.csv";
            if (cbJSON.Checked)
            {
                saveFileDialog1.Filter = "JSON File|*.json";
            }
            saveFileDialog1.Title = "Where do you want to save the file?";
            saveFileDialog1.ShowDialog();
            if(saveFileDialog1.FileName != "")
            {
                txtSaveFile.Text = saveFileDialog1.FileName;
            }

        }
    }
}
